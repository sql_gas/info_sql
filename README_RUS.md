# Info

Проект посвящен анализу данных, на примере бд о студентах.


## Part 1. Создание базы данных

Скрипт создаёт базу данных и таблицы. Ниже написаны процедуры, позволяющие импортировать и экспортировать данные.

## Part 2. Изменение данных

Скрипт содержит следующие процедуы: добавление проверки проекта, добавление проверки проекта автотестом.
Также написаны триггеры, на изменение данных в таблицах.

## Part 3. Получение данных

##### 1) Необходимо создать функцию, которая вернет таблицу TransferredPoints в более понятном и удобном для чтения виде.

##### 2) Необходимо создать функцию, которая будет возвращать таблицу с тремя столбцами: ник пользователя, название проверенного задания и количество полученных очков опыта

##### 3) Нужно написать функцию, которая будет определять тех пользователей, которые не покинули кампус в течение всего дня.

##### 4) Требуется найти процентное соотношение успешных и неуспешных проверок за всё время.
           
##### 5) Необходимо рассчитать изменение количества пир-баллов для каждого пира на основе таблицы TransferredPoints.

##### 6) Требуется рассчитать изменение количества пир-баллов для каждого пира на основе таблицы, которая будет возвращена.

##### 7) Необходимо определить наиболее часто проверяемое задание за каждый день.

##### 8) Длительность последней проверки

##### 9) Требуется найти всех пиров, которые выполнили все задачи в заданном блоке и определить дату завершения последней задачи.

##### 10) Требуется определить, какому пиру должен идти каждый обучающийся на проверку.

##### 11) Количество пиров, которые приступили к блокам заданий

##### 12) Необходимо найти N пиров с наибольшим количеством друзей.

##### 13) Требуется определить процент пиров, которые когда-либо успешно проходили проверку в день своего рождения.

##### 14) Требуется определить общее количество очков опыта (XP), полученных каждым пиром в сумме.

##### 15) Необходимо определить всех пиров, которые сдали задания 1 и 2, но не сдали задание 3.

##### 16) Требуется с помощью рекурсивного обобщенного табличного выражения вывести количество задач, предшествующих каждой из них.

##### 17) Требуется найти "удачные" для проверок дни, то есть дни, в которых произошло как минимум N успешных проверок подряд.

##### 18) Требуется определить пира с наибольшим количеством выполненных заданий.

##### 19) Необходимо определить пира с наибольшим количеством очков опыта (XP).

##### 20) Требуется определить пира, который провел в кампусе больше всего времени сегодня.

##### 21) Необходимо определить пиров, которые приходили раньше заданного времени не менее N раз за все время.

##### 22) Требуется найти пиров, которые выходили из кампуса более M раз за последние N дней.

##### 23) Требуется вывести ник пира, кто пришел крайним.

##### 24) Пиры, вышедшие вчера больше, чем на N минут

##### 25) Вывести для каждого месяца процент раннего входа. 

## Part 4. Метаданные

##### 1) Написать хранимую процедуру, которая без удаления базы данных удаляет все таблицы текущей базы данных, имена которых начинаются со словосочетания "TableName".

##### 2) Разработать хранимую процедуру с выходным параметром, которая выводит список имен и параметров всех скалярных SQL функций пользователя в текущей базе данных. При выводе имена функций без параметров должны быть исключены. Имена и список параметров должны быть представлены в одной строке. Кроме того, выходной параметр должен содержать количество найденных функций.

##### 3) Реализовать хранимую процедуру с выходным параметром, которая удаляет все SQL DML триггеры в текущей базе данных. Выходной параметр должен содержать количество удаленных триггеров.

##### 4) Создать хранимую процедуру с входным параметром, которая выводит имена и описания типов объектов (только хранимых процедур и скалярных функций), в тексте которых на языке SQL встречается строка, задаваемая входным параметром.