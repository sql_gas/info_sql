-- CREATE DATABASE INFO21;
DROP TABLE IF EXISTS Peers;
CREATE  TABLE Peers
(
    Nickname VARCHAR PRIMARY KEY,
    Birthday DATE
);
DROP TABLE IF EXISTS Tasks;
CREATE TABLE Tasks
(
    Title      VARCHAR PRIMARY KEY,
    ParentTask VARCHAR,
    MaxXP      INTEGER
);
DROP TYPE IF EXISTS State cascade;
CREATE TYPE State AS enum ('Start', 'Success', 'Failure');
DROP TABLE IF EXISTS P2P;
CREATE TABLE P2P
(
    id           SERIAL PRIMARY KEY,
    "Check"      INTEGER,
    CheckingPeer VARCHAR,
    State        State,
    Time         TIME
);
DROP TABLE IF EXISTS Verter;
CREATE TABLE Verter
(
    id      SERIAL PRIMARY KEY,
    "Check" INTEGER NOT NULL,
    "State" State   NOT NULL,
    Time    TIME    NOT NULL
);
DROP TABLE IF EXISTS Checks;
CREATE TABLE Checks
(
    id   SERIAL PRIMARY KEY,
    Peer VARCHAR,
    Task VARCHAR,
    Date DATE
);
DROP TABLE IF EXISTS TransferredPoints;
CREATE TABLE TransferredPoints
(
    id           SERIAL PRIMARY KEY,
    CheckingPeer VARCHAR,
    CheckedPeer  VARCHAR,
    PointsAmount INTEGER
);
DROP TABLE IF EXISTS Friends;
CREATE TABLE Friends
(
    id    SERIAL PRIMARY KEY,
    Peer1 VARCHAR,
    Peer2 VARCHAR
);
DROP TABLE IF EXISTS Recommendations;
CREATE TABLE Recommendations
(
    id              bigint PRIMARY KEY,
    Peer            VARCHAR,
    RecommendedPeer VARCHAR
);
DROP TABLE IF EXISTS XP;
CREATE TABLE XP
(
    id       SERIAL PRIMARY KEY,
    "Check"  INTEGER,
    XPAmount INTEGER
);
DROP TYPE IF EXISTS StateVisit cascade;
CREATE TYPE StateVisit AS enum ('1', '2');
DROP TABLE IF EXISTS TimeTracking;
CREATE TABLE TimeTracking
(
    id      bigint PRIMARY KEY,
    Peer    VARCHAR,
    Date    DATE,
    Time    TIME,
    "State" StateVisit
);

---- linux
-- CREATE OR REPLACE PROCEDURE csv_import(tab VARCHAR, filepath VARCHAR, delim VARCHAR)
-- AS
-- $$
-- BEGIN
--     EXECUTE FORMAT('COPY %s FROM %s WITH (delimiter %s, format csv, header);', tab, filepath, delim);
-- END;
-- $$ LANGUAGE plpgsql;
--
-- CALL csv_import('peers', '''/tmp/csv_files/peers.csv''', ''',''');
-- CALL csv_import('recommendations', '''/tmp/csv_files/recommendations.csv''', ''',''');
-- CALL csv_import('checks', '''/tmp/csv_files/checks.csv''', ''',''');
-- CALL csv_import('friends', '''/tmp/csv_files/friends.csv''', ''',''');
-- CALL csv_import('p2p', '''/tmp/csv_files/p2p.csv''', ''',''');
-- CALL csv_import('tasks', '''/tmp/csv_files/tasks.csv''', ''',''');
-- CALL csv_import('timetracking', '''/tmp/csv_files/timetracking.csv''', ''',''');
-- CALL csv_import('transferredpoints', '''/tmp/csv_files/transferredpoints.csv''', ''',''');
-- CALL csv_import('verter', '''/tmp/csv_files/verter.csv''', ''',''');
-- CALL csv_import('xp', '''/tmp/csv_files/xp.csv''', ''',''');

----windows
CREATE OR REPLACE PROCEDURE csv_import(tab VARCHAR, filepath VARCHAR, delim VARCHAR)
AS
$$
BEGIN
    EXECUTE FORMAT('COPY %s FROM %s WITH (delimiter %s, format csv, header);', tab, filepath, delim);
END;
$$ LANGUAGE plpgsql;

CALL csv_import('peers', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\peers.csv''', ''',''');
CALL csv_import('recommendations', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\recommendations.csv''',
                ''',''');
CALL csv_import('checks', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\checks.csv''', ''',''');
CALL csv_import('friends', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\friends.csv''', ''',''');
CALL csv_import('p2p', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\p2p.csv''', ''',''');
CALL csv_import('tasks', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\tasks.csv''', ''',''');
CALL csv_import('timetracking', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\timetracking.csv''',
                ''',''');
CALL csv_import('transferredpoints',
                '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\transferredpoints.csv''', ''',''');
CALL csv_import('verter', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\verter.csv''', ''',''');
CALL csv_import('xp', '''C:\\Users\\17079\\Desktop\\myrepo\\info_sql\\csv_files\\xp.csv''', ''',''');

---- linux
-- CREATE OR REPLACE PROCEDURE csv_export(tab VARCHAR, filepath VARCHAR, delim VARCHAR)
-- AS
-- $$
-- BEGIN
--     EXECUTE FORMAT('COPY %s TO %s WITH (delimiter %s, format csv, header);', tab, filepath, delim);
-- END;
-- $$ LANGUAGE plpgsql;