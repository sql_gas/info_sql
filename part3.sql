--1
CREATE
    OR REPLACE FUNCTION TransferredPoints_t()
    RETURNS TABLE
            (
                Peer1        varchar,
                Peer2        varchar,
                pointsamount int
            )
AS
$$
SELECT Peer1,
       Peer2,
       - pointsamount
FROM (SELECT id,
             checkingpeer          AS Peer1,
             checkedpeer           AS Peer2,
             pointsamount + p_temp as pointsamount
      FROM TransferredPoints
               INNER JOIN(SELECT id                    as i,
                                 checkingpeer          AS Peer2,
                                 checkedpeer           AS Peer1,
                                 (pointsamount * (-1)) AS p_temp
                          FROM TransferredPoints) AS t ON checkingpeer = t.Peer1
          AND checkedpeer = t.Peer2
      WHERE id < i
      UNION ALL
      SELECT id,
             checkingpeer AS Peer1,
             checkedpeer  AS Peer2,
             pointsamount
      FROM TransferredPoints
               LEFT JOIN(SELECT id                    as i,
                                checkingpeer          AS Peer2,
                                checkedpeer           AS Peer1,
                                (pointsamount * (-1)) AS p_temp
                         FROM TransferredPoints) AS t ON checkingpeer = t.Peer1
          AND checkedpeer = t.Peer2
      WHERE (
                    checkingpeer IS NULL
                    OR t.Peer1 IS NULL
                )
      ORDER BY id) as t
$$ LANGUAGE SQL;
SELECT *
FROM TransferredPoints_t();
--2
CREATE
    OR REPLACE FUNCTION part3_2()
    RETURNS TABLE
            (
                Peer varchar,
                Task varchar,
                XP   int
            )
AS
$$
SELECT Peer,
       Task,
       XP.XPAmount
FROM Checks
         LEFT JOIN XP ON Checks.id = XP."Check"
WHERE xp IS NOT NULL
$$ LANGUAGE SQL;
SELECT *
FROM part3_2();
--3
CREATE
    OR REPLACE FUNCTION part3_3(day_t date)
    RETURNS TABLE
            (
                Peer varchar
            )
AS
$$
SELECT Peer
FROM (SELECT Peer,
             COUNT(id)
      FROM timetracking
      WHERE state = '1'
        AND Date = day_t
      GROUP BY Peer) AS t
WHERE count = 1
$$ LANGUAGE SQL;
SELECT *
FROM part3_3('2022-07-04');
--4
CREATE
    OR REPLACE PROCEDURE part3_4(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH success_p2p AS (
        SELECT count(*) sp
        FROM p2p
        WHERE State = 'Success'
    ),
                              all_p2p AS (
                                  SELECT count(*) ap
                                  FROM p2p
                                  WHERE State = 'Start'
                              ),
                              all_verter AS (
                                  SELECT count(*) av
                                  FROM Verter
                                  WHERE State = 'Start'
                              ),
                              success_verter AS (
                                  SELECT count(*) sv
                                  FROM Verter
                                  WHERE State = 'Success'
                              )
                         SELECT (sp + sv) * 100 / (ap + av)       SuccessfulChecks,
                                100 - (sp + sv) * 100 / (ap + av) UnsuccessfulChecks
                         FROM success_p2p,
                              all_p2p,
                              all_verter,
                              success_verter;
END
$$;
BEGIN;
CALL part3_4();
FETCH ALL FROM "rs_resultone";
END;
--5
CREATE
    OR REPLACE PROCEDURE part3_5(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH oput as (
        SELECT checkingpeer        AS Peer,
               - SUM(pointsamount) as p
        FROM transferredpoints
        GROUP BY checkingpeer
    ),
                              iput as (
                                  SELECT checkedpeer       AS Peer,
                                         SUM(pointsamount) as p
                                  FROM transferredpoints
                                  GROUP BY checkedpeer
                              )
                         SELECT Peer,
                                SUM(p) AS PointsChange
                         FROM (SELECT Peer,
                                      p
                               FROM oput
                               UNION
                               SELECT Peer,
                                      p
                               FROM iput
                               ORDER BY Peer) as t
                         GROUP BY Peer
                         ORDER BY PointsChange DESC;
END
$$;
BEGIN;
CALL part3_5();
FETCH ALL FROM "rs_resultone";
END;
--6
CREATE
    OR REPLACE PROCEDURE part3_6(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH iput as (
        SELECT peer1        as Peer,
               pointsamount as p
        FROM TransferredPoints_t()
    ),
                              oput as (
                                  SELECT peer2          AS Peer,
                                         - pointsamount as p
                                  FROM TransferredPoints_t()
                              )
                         SELECT Peer,
                                SUM(p) as PointsChange
                         FROM (SELECT Peer,
                                      p
                               FROM oput
                               UNION ALL
                               SELECT Peer,
                                      p
                               FROM iput) as t
                         GROUP BY Peer
                         ORDER BY PointsChange DESC;
END
$$;
BEGIN;
CALL part3_6();
FETCH ALL FROM "rs_resultone";
END;
--7
CREATE
    OR REPLACE PROCEDURE part3_7(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH temp AS (
        SELECT Checks."date" as day,
               Task,
               (COUNT(*))    as c
        FROM Checks
        GROUP BY Checks."date",
                 Task
    ),
                              max_in_day AS (
                                  SELECT day,
                                         MAX(c) as m
                                  FROM Temp
                                  GROUP BY day
                              )
                         SELECT to_char(temp.day, 'DD.MM.YYYY'),
                                Task
                         FROM temp
                                  RIGHT JOIN max_in_day ON c = m
                             AND temp.day = max_in_day.day;
END
$$;
BEGIN;
CALL part3_7();
FETCH ALL FROM "rs_resultone";
END;
--8
CREATE
    OR REPLACE PROCEDURE part3_8(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH id_t AS (
        SELECT MAX("Check") id_temp
        FROM P2P
        WHERE State = 'Success'
           OR State = 'Failure'
    ),
                              time_t AS (
                                  SELECT "time" time_temp
                                  FROM P2P,
                                       id_t
                                  WHERE (
                                          State = 'Success'
                                          OR State = 'Failure'
                                      )
                                    AND "Check" = id_temp
                              ),
                              temp AS (
                                  SELECT "time" timing
                                  FROM P2P,
                                       id_t
                                  WHERE "Check" = id_temp
                                    AND State = 'Start'
                              )
                         SELECT (time_temp - timing) ::time timing
                         FROM time_t,
                              temp;
END
$$;
BEGIN;
CALL part3_8();
FETCH ALL FROM "rs_resultone";
END;
--9
CREATE
    OR REPLACE PROCEDURE part3_9(_block varchar,
                                 INOUT _result_one refcursor = 'rs_resultone')
    LANGUAGE plpgsql AS
$$
BEGIN
    _block := '(' || _block || '+[0-9].*)';
    OPEN _result_one FOR WITH temp as (
        SELECT title
        FROM tasks
        WHERE title ~ _block
    ),
                              co AS (
                                  SELECT count(*) as count_c
                                  FROM temp
                              ),
                              all_task_peer AS (
                                  SELECT DISTINCT Peer,
                                                  task,
                                                  Checks.date
                                  FROM Checks
                                           RIGHT JOIN temp ON Checks.task = Temp.title
                              ),
                              group_date AS (
                                  SELECT Peer,
                                         max(date) AS Day,
                                         count(*)  as count_d
                                  from all_task_peer
                                  GROUP BY Peer
                              )
                         SELECT Peer,
                                to_char(Day, 'DD.MM.YYYY')
                         FROM group_date,
                              co
                         WHERE group_date.count_d = co.count_c;
END
$$;
BEGIN;
CALL part3_9('C');
FETCH ALL FROM "rs_resultone";
END;
--10
CREATE
    OR REPLACE PROCEDURE part3_10(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH temp as (
        SELECT peer1,
               recommendedpeer,
               count(*) c
        FROM friends
                 JOIN recommendations ON peer2 = peer
        WHERE peer1 != recommendedpeer
        GROUP BY peer1,
                 recommendedpeer
        ORDER BY peer1
    ),
                              max_c as (
                                  SELECT peer1,
                                         max(c) m
                                  FROM temp
                                  GROUP BY Peer1
                              )
                         SELECT t.Peer1 Peer,
                                t.RecommendedPeer
                         FROM temp t
                                  JOIN max_c m ON t.Peer1 = m.Peer1
                             AND t.c = m.m;
END
$$;
BEGIN;
CALL part3_10();
FETCH ALL FROM "rs_resultone";
END;
--11
CREATE
    OR REPLACE PROCEDURE part3_11(_block1 varchar,
                                  _block2 varchar,
                                  INOUT _result_one refcursor = 'rs_resultone')
    LANGUAGE plpgsql AS
$$
BEGIN
    _block1 := '(' || _block1 || '+[0-9].*)';
    _block2
        := '(' || _block2 || '+[0-9].*)';
    OPEN _result_one FOR WITH block1 as (
        SELECT title
        FROM tasks
        WHERE title ~ _block1
    ),
                              block2 as (
                                  SELECT title
                                  FROM tasks
                                  WHERE title ~ _block2
                              ),
                              co AS (
                                  SELECT row_number() OVER () as count_c
                                  FROM peers
                              ),
                              block1_peer AS (
                                  SELECT DISTINCT row_number() OVER () as count_b1,
                                                  Peer
                                  FROM Checks
                                           RIGHT JOIN block1 ON Checks.task = block1.title
                                  GROUP BY Peer
                              ),
                              block2_peer AS (
                                  SELECT DISTINCT row_number() OVER () as count_b2,
                                                  Peer
                                  FROM Checks
                                           RIGHT JOIN block2 ON Checks.task = block2.title
                                  GROUP BY Peer
                              ),
                              both_block AS (
                                  SELECT DISTINCT row_number() OVER () as count_both,
                                                  b1.Peer
                                  FROM block1_peer b1
                                           INNER JOIN block2_peer b2 ON b1.Peer = b2.Peer
                                  GROUP BY b1.Peer
                              ),
                              only_block1_peer AS (
                                  SELECT DISTINCT row_number() OVER () as count_bb1,
                                                  b1.Peer
                                  FROM block1_peer b1
                                           LEFT JOIN both_block b ON b1.Peer = b.Peer
                                  WHERE b.Peer IS NULL
                                  GROUP BY b1.Peer
                              ),
                              only_block2_peer AS (
                                  SELECT DISTINCT row_number() OVER () as count_bb2,
                                                  b2.Peer
                                  FROM block2_peer b2
                                           LEFT JOIN both_block b ON b2.Peer = b.Peer
                                  WHERE b.Peer IS NULL
                                  GROUP BY b2.Peer
                              ),
                              check_null1 AS (
                                  SELECT CASE
                                             WHEN MAX(count_bb1) IS NULL THEN 0
                                             ELSE MAX(count_bb1) * 100 / MAX(count_c)
                                             END AS StartedBlock1
                                  FROM only_block1_peer,
                                       co
                              ),
                              check_null2 AS (
                                  SELECT CASE
                                             WHEN MAX(count_bb2) IS NULL THEN 0
                                             ELSE MAX(count_bb2) * 100 / MAX(count_c)
                                             END AS StartedBlock2
                                  FROM only_block2_peer,
                                       co
                              ),
                              check_null_both AS (
                                  SELECT CASE
                                             WHEN MAX(count_both) IS NULL THEN 0
                                             ELSE MAX(count_both) * 100 / MAX(count_c)
                                             END AS StartedBothBlocks
                                  FROM both_block,
                                       co
                              )
                         SELECT *,
                                (
                                    100 - StartedBlock1 - StartedBlock2 - StartedBothBlocks
                                    ) DidntStartAnyBlock
                         FROM check_null1,
                              check_null2,
                              check_null_both;
END
$$;
BEGIN;
CALL part3_11('C', 'CPP');
FETCH ALL FROM "rs_resultone";
END;
--12
CREATE
    OR REPLACE PROCEDURE part3_12(N INT,
                                  INOUT _result_one refcursor = 'rs_resultone')
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR
        SELECT peer1    as Peer,
               COUNT(*) as FriendsCount
        FROM Friends
        GROUP BY peer1
        ORDER BY FriendsCount DESC
        LIMIT N;
END
$$;
BEGIN;
CALL part3_12(2);
FETCH ALL FROM "rs_resultone";
END;
--13
CREATE
    OR REPLACE PROCEDURE part3_13(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH bfday_peers AS (
        SELECT nickname,
               EXTRACT(
                       DAY
                       FROM birthday
                   ) as "day",
               EXTRACT(
                       MONTH
                       FROM birthday
                   )    "month"
        FROM Peers
    ),
                              check_day AS (
                                  SELECT id,
                                         peer,
                                         EXTRACT(
                                                 DAY
                                                 FROM date
                                             ) as "day",
                                         EXTRACT(
                                                 MONTH
                                                 FROM date
                                             )    "month"
                                  FROM Checks
                              ),
                              needs_peer AS (
                                  SELECT id,
                                         peer
                                  FROM check_day c
                                           INNER JOIN bfday_peers b ON c."day" = b."day"
                                      AND c."month" = b."month"
                              ),
                              count_p AS (
                                  SELECT row_number() OVER () as c_p,
                                         Peer
                                  FROM (
                                           SELECT DISTINCT Peer
                                           FROM needs_peer
                                       ) t
                              ),
                              success_p2p AS (
                                  SELECT n.id,
                                         n.peer,
                                         State
                                  FROM p2p p
                                           RIGHT JOIN needs_peer n ON p."Check" = n.id
                                  WHERE State = 'Success'
                              ),
                              success_no_verter AS (
                                  SELECT sp.id,
                                         sp.peer,
                                         sp.State
                                  FROM success_p2p sp
                                           LEFT JOIN Verter v ON sp.id = v."Check"
                                  WHERE v."Check" IS NULL
                              ),
                              success_verter AS (
                                  SELECT sp.id,
                                         sp.peer,
                                         v.State
                                  FROM success_p2p sp
                                           JOIN Verter v ON sp.id = v."Check"
                                  WHERE v.State = 'Success'
                              ),
                              success_peer AS (
                                  SELECT row_number() OVER () i,
                                         Peer
                                  FROM (
                                           SELECT Peer
                                           FROM success_no_verter
                                           UNION
                                           SELECT Peer
                                           FROM success_verter
                                       ) t
                              )
                         SELECT MAX(i) * 100 / MAX(c_p)         SuccessfulChecks,
                                (100 - MAX(i) * 100 / MAX(c_p)) UnsuccessfulChecks
                         FROM success_peer,
                              count_p;
END
$$;
BEGIN;
CALL part3_13();
FETCH ALL FROM "rs_resultone";
END;
--14
CREATE
    OR REPLACE PROCEDURE part3_14(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR
        SELECT peer,
               SUM(XP) as XP
        FROM (SELECT peer,
                     task,
                     MAX(XPAmount) AS XP
              FROM Checks
                       JOIN XP ON Checks.id = XP."Check"
              GROUP BY peer,
                       task) as temp
        GROUP BY peer
        ORDER BY XP DESC;
END
$$;
BEGIN;
CALL part3_14();
FETCH ALL FROM "rs_resultone";
END;
--15
CREATE
    OR REPLACE PROCEDURE part3_15(_task1 varchar,
                                  _task2 varchar,
                                  _task3 varchar,
                                  INOUT _result_one refcursor = 'rs_resultone')
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH task1 AS (
        SELECT title
        FROM tasks
        WHERE title ~ _task1
    ),
                              task2 AS (
                                  SELECT title
                                  FROM tasks
                                  WHERE title ~ _task2
                              ),
                              task3 AS (
                                  SELECT title
                                  FROM tasks
                                  WHERE title ~ _task3
                              ),
                              check_t1 AS (
                                  SELECT id,
                                         peer
                                  FROM Checks,
                                       task1 t
                                  WHERE task = t.title
                              ),
                              check_t2 AS (
                                  SELECT id,
                                         peer
                                  FROM Checks,
                                       task2 t
                                  WHERE task = t.title
                              ),
                              check_t3 AS (
                                  SELECT id,
                                         peer
                                  FROM Checks,
                                       task3 t
                                  WHERE task = t.title
                              ),
                              peer_t3 AS (
                                  SELECT nickname AS Peer
                                  FROM Peers p
                                           LEFT JOIN (
                                      SELECT Peer
                                      FROM check_t3 c
                                               JOIN xp ON c.id = xp."Check"
                                  ) t ON p.nickname = t.Peer
                                  WHERE t.Peer IS NULL
                              )
                         SELECT t.Peer
                         FROM (SELECT t1.Peer
                               FROM (SELECT Peer
                                     FROM check_t1 c
                                              JOIN xp ON c.id = xp."Check") t1
                                        JOIN (SELECT Peer
                                              FROM check_t2 c
                                                       JOIN xp ON c.id = xp."Check") t2 ON t1.Peer = t2.Peer) t
                                  JOIN peer_t3 p ON t.Peer = p.Peer;
END
$$;
BEGIN;
CALL part3_15(
        'C3_SimpleBashUtils',
        'C2_s21_stringplus',
        'C5_s21_decimal'
    );
FETCH ALL FROM "rs_resultone";
END;
--16
CREATE
    OR REPLACE PROCEDURE part3_16(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH RECURSIVE
                             t(title, parenttask) AS (
                                 SELECT t1.title,
                                        t1.parenttask,
                                        0 AS i
                                 FROM tasks t1
                                 UNION ALL
                                 SELECT t2.title,
                                        t2.parenttask,
                                        t.i + 1
                                 FROM tasks t2
                                          JOIN t ON t.parenttask = t2.title
                             ),
                             N AS (
                                 SELECT (count(*) - 2) n
                                 FROM tasks
                             )
                         SELECT Task,
                                n - i PrevCount
                         FROM (SELECT MAX(i) i,
                                      title  Task
                               FROM t
                               GROUP BY title) t,
                              N;
END
$$;
BEGIN;
CALL part3_16();
FETCH ALL FROM "rs_resultone";
END;
--17
CREATE
    OR REPLACE FUNCTION check_day(day DATE, n INT) RETURNS BOOLEAN AS
$$
DECLARE
    temprow RECORD;
    cnt
            INT := 0;
BEGIN
    FOR temprow IN (
        SELECT c.id,
               p.State,
               v.State,
               c.task,
               c.date,
               x.xpamount,
               t.maxxp
        FROM checks c
                 JOIN p2p p ON p."Check" = c.id
                 JOIN xp x ON p."Check" = x."Check"
                 JOIN verter v ON p."Check" = v."Check"
                 JOIN tasks t ON c.task = t.title
        WHERE p.State != 'Start'
          AND v.State != 'Start'
          AND c.date = day
    )
        LOOP
            IF temprow.State = 'Success'
                AND (
                       temprow.State = 'Success'
                       OR temprow.State IS NULL
                   )
                AND temprow.xpamount / temprow.maxxp >= 0.8 THEN
                cnt = cnt + 1;
            ELSE
                cnt := 0;
            END IF;
            IF
                cnt = n THEN
                RETURN TRUE;
            END IF;
        END LOOP;
    RETURN FALSE;
END
$$
    LANGUAGE plpgsql;
CREATE
    OR REPLACE PROCEDURE best_check_days(n INT, INOUT days refcursor) AS
$$
BEGIN
    OPEN days FOR (
        SELECT DISTINCT date
        FROM checks
        WHERE check_day(date::date, n)
    );
END
$$
    LANGUAGE plpgsql;
BEGIN;
CALL best_check_days(1, 'days');
FETCH ALL FROM "days";
COMMIT;
--18
CREATE
    OR REPLACE PROCEDURE part3_14(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR
        SELECT peer,
               COUNT(XP) as XP
        FROM (SELECT peer,
                     task,
                     MAX(XPAmount) AS XP
              FROM Checks
                       JOIN XP ON Checks.id = XP."Check"
              GROUP BY peer,
                       task) as temp
        GROUP BY peer
        ORDER BY XP DESC
        LIMIT 1;
END
$$;
BEGIN;
CALL part3_14();
FETCH ALL FROM "rs_resultone";
END;
--19
CREATE
    OR REPLACE PROCEDURE part3_19(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR
        SELECT Peer,
               SUM(xpamount) XP
        FROM checks c
                 JOIN xp x ON c.id = x."Check"
        GROUP BY peer
        ORDER BY XP DESC
        LIMIT 1;
END
$$;
BEGIN;
CALL part3_19();
FETCH ALL FROM "rs_resultone";
END;
--20
CREATE
    OR REPLACE PROCEDURE part3_20(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH intime AS (
        SELECT id,
               peer,
               Time
        FROM TimeTracking
        WHERE (TimeTracking.Date = CURRENT_DATE)
          AND TimeTracking.State = '1'
        ORDER BY id
    ),
                              outtime AS (
                                  SELECT id,
                                         peer,
                                         Time
                                  FROM TimeTracking
                                  WHERE (TimeTracking.Date = CURRENT_DATE)
                                    AND TimeTracking.State = '2'
                                  ORDER BY id
                              ),
                              JOINTable AS (
                                  SELECT DISTINCT ON (i.id) i.id   AS id1,
                                                            i.peer AS peer1,
                                                            i.Time AS time1,
                                                            o.id   AS id2,
                                                            o.peer AS peer2,
                                                            o.Time as time2
                                  FROM intime i
                                           INNER JOIN outtime o ON i.peer = o.peer
                                      AND i.Time < o.Time
                                  ORDER BY 1,
                                           2,
                                           3,
                                           6
                              ),
                              peerandmaxtime AS (
                                  SELECT peer1 as Peer
                                  FROM JOINTable
                                  GROUP BY peer1
                                  HAVING SUM(time2 - time1)::time = (
                                      SELECT(SUM(time2 - time1)::time) AS time3
                                      FROM JOINtable
                                      GROUP BY peer1
                                      ORDER BY 1 DESC
                                      LIMIT 1
                                  )
                              )
                         SELECT *
                         FROM peerandmaxtime;
END
$$;
BEGIN;
CALL part3_20();
FETCH ALL FROM "rs_resultone";
END;
--21
CREATE
    OR REPLACE PROCEDURE part3_21(_time time,
                                  _n int,
                                  INOUT _result_one refcursor = 'rs_resultone')
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR
        SELECT peer
        FROM (SELECT peer,
                     count(*) c
              FROM timetracking
              WHERE State = '1'
                AND "time" < _time
              GROUP BY peer) t
        WHERE c >= _n;
END
$$;
BEGIN;
CALL part3_21('05:00:00', 1);
FETCH ALL FROM "rs_resultone";
END;
--22
CREATE
    OR REPLACE PROCEDURE part3_22(count_d int,
                                  cout_M int,
                                  INOUT _result_one refcursor = 'rs_resultone')
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH temp_day AS (
        SELECT *
        FROM timetracking
        WHERE (Date >= CURRENT_DATE - count_d)
          AND State = '2'
    ),
                              temp_peer AS (
                                  SELECT Peer,
                                         COUNT(id) as c
                                  FROM temp_day
                                  GROUP BY Peer
                              )
                         SELECT Peer
                         FROM temp_peer
                         WHERE c > cout_M;
END
$$;
BEGIN;
CALL part3_22(3, 2);
FETCH ALL FROM "rs_resultone";
END;
--23
CREATE
    OR REPLACE PROCEDURE part3_23(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR
        SELECT Peer
        FROM timetracking
        WHERE "date" = CURRENT_DATE
        ORDER BY "time" DESC
        LIMIT 1;
END
$$;
BEGIN;
CALL part3_23();
FETCH ALL FROM "rs_resultone";
END;
--24
CREATE
    OR REPLACE PROCEDURE part3_24(count_N int,
                                  INOUT _result_one refcursor = 'rs_resultone')
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR -- WITH temp_day AS (
        WITH interval AS (
            SELECT count_N * '1 minute'::interval c
        ),
             _in AS (
                 SELECT id,
                        Peer,
                        time i
                 FROM timetracking
                 WHERE (Date = CURRENT_DATE - 1)
                   AND State = '1'
             ),
             _out AS (
                 SELECT id,
                        Peer,
                        time o
                 FROM timetracking
                 WHERE (Date = CURRENT_DATE - 1)
                   AND State = '2'
             ),
             need_table AS (
                 SELECT *
                 FROM (
                          SELECT i.id  in_id,
                                 LAG(i.id) OVER (
                                     PARTITION BY i.Peer
                                     ORDER BY i.Peer
                                     ) t,
                                 o.id  out_id,
                                 i.Peer,
                                 i,
                                 o
                          FROM _out o
                                   JOIN _in i ON o.peer = i.peer
                          WHERE i.id > o.id
                      ) t
                 WHERE (t IS NULL)
                    OR t > in_id
                    OR t = in_id
             ),
             time_out AS (
                 SELECT Peer,
                        SUM(i - o)::time t_out
                 FROM need_table
                 GROUP BY Peer
             )
        SELECT Peer
        FROM time_out,
             interval
        WHERE t_out > c;
END
$$;
BEGIN;
CALL part3_24(300);
FETCH ALL FROM "rs_resultone";
END;
--25
CREATE
    OR REPLACE PROCEDURE part3_25(
    INOUT _result_one refcursor = 'rs_resultone'
)
    LANGUAGE plpgsql AS
$$
BEGIN
    OPEN _result_one FOR WITH n_month AS (
        SELECT to_char("gs", 'Month') "Month",
               EXTRACT(
                       MONTH
                       FROM "gs"
                   ) as               mn
        FROM (
                 SELECT gs::date
                 FROM generate_series('2022-01-01', '2022-12-01', interval '1 month') as gs
             ) t
    ),
                              invite_peer AS (
                                  SELECT nickname,
                                         EXTRACT(
                                                 MONTH
                                                 FROM "birthday"
                                             ) m,
                                         time
                                  FROM peers p
                                           JOIN timetracking t ON p.nickname = t.peer
                                  WHERE State = '1'
                                    AND EXTRACT(
                                                MONTH
                                                FROM "birthday"
                                            ) = EXTRACT(
                                                MONTH
                                                FROM "date"
                                            )
                              ),
                              main_in AS (
                                  SELECT COUNT(*) c_main,
                                         m
                                  FROM invite_peer
                                  GROUP BY m
                              ),
                              early_in AS (
                                  SELECT COUNT(*) c_early,
                                         m        mo
                                  FROM invite_peer
                                  WHERE "time" < '12:00:00'
                                  GROUP BY m
                              )
                         SELECT "Month",
                                EarlyEntries
                         FROM n_month n
                                  LEFT JOIN (SELECT c_early * 100 / c_main EarlyEntries,
                                                    m
                                             FROM early_in e
                                                      JOIN main_in m ON e.mo = m.m) t ON n.mn = t.m;
END
$$;
BEGIN;
CALL part3_25();
FETCH ALL FROM "rs_resultone";
END;