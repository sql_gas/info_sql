CREATE DATABASE part4;
CREATE TABLE TableName_1 (
    id SERIAL PRIMARY KEY,
    Peer1 VARCHAR,
    Peer2 VARCHAR
);

CREATE TABLE TableName_2 (
    id SERIAL PRIMARY KEY,
    Peer VARCHAR,
    RecommendedPeer VARCHAR
);

CREATE TABLE Checks (
    id SERIAL PRIMARY KEY,
    Peer VARCHAR,
    Task VARCHAR,
    Check_date DATE
);

CREATE TABLE tablename1_audit (
    created timestamp with time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'Asia/Novosibirsk'),
    type_event char(1) NOT NULL
        DEFAULT 'I',
    row_id BIGINT NOT NULL,
    Peer1 VARCHAR,
    Peer2 VARCHAR);

ALTER TABLE tablename1_audit ADD CONSTRAINT ch_type_event CHECK (type_event IN ('I','U', 'D'));

CREATE TABLE tablename2_audit (
    created timestamp with time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'Asia/Novosibirsk'),
    type_event char(1) NOT NULL
        DEFAULT 'I',
    row_id BIGINT NOT NULL,
    Peer1 VARCHAR,
    Peer2 VARCHAR);

ALTER TABLE tablename2_audit ADD CONSTRAINT ch_type_event CHECK (type_event IN ('I','U', 'D'));


INSERT INTO TableName_1(Peer1, Peer2) VALUES('Aboba', 'Alfa');
INSERT INTO TableName_1(Peer1, Peer2) VALUES('Alfa', 'Aboba');
INSERT INTO TableName_1(Peer1, Peer2) VALUES('Gamma', 'Delta');
INSERT INTO TableName_1(Peer1, Peer2) VALUES('Delta', 'Gamma');
INSERT INTO TableName_1(Peer1, Peer2) VALUES('Beta', 'Kappa');

INSERT INTO TableName_2(Peer, RecommendedPeer) VALUES('Aboba', 'Alfa');
INSERT INTO TableName_2(Peer, RecommendedPeer) VALUES('Alfa', 'Beta');
INSERT INTO TableName_2(Peer, RecommendedPeer) VALUES('Beta', 'Aboba');
INSERT INTO TableName_2(Peer, RecommendedPeer) VALUES('Kappa', 'Gamma');
INSERT INTO TableName_2(Peer, RecommendedPeer) VALUES('Gamma', 'Alfa');

INSERT INTO Checks(Peer, Task, Check_date) VALUES('Alfa', 'C3_SimpleBashUtils', '2022-10-12');
INSERT INTO Checks(Peer, Task, Check_date) VALUES('Beta', 'C3_SimpleBashUtils', '2022-10-15');
INSERT INTO Checks(Peer, Task, Check_date) VALUES('Aboba', 'C3_SimpleBashUtils', '2022-10-16');
INSERT INTO Checks(Peer, Task, Check_date) VALUES('Alfa', 'C2_s21_stringplus', '2022-10-17');
INSERT INTO Checks(Peer, Task, Check_date) VALUES('Gamma', 'C2_s21_stringplus', '2022-10-18');



CREATE FUNCTION fnc_trg_tablename1_insert()
    RETURNS TRIGGER
    AS $tablename1_audit$
BEGIN
  IF (TG_OP = 'INSERT') THEN
        INSERT INTO tablename1_audit SELECT now(), 'I', NEW.*;
  END IF;
  RETURN NULL;
END;
$tablename1_audit$ LANGUAGE plpgsql;

CREATE FUNCTION fnc_trg_tablename1_delete()
    RETURNS TRIGGER
    AS $tablename1_audit$
BEGIN
  IF (TG_OP = 'DELETE') THEN
        INSERT INTO tablename1_audit SELECT now(), 'D', OLD.*;
  END IF;
  RETURN NULL;
END;
$tablename1_audit$ LANGUAGE plpgsql;

CREATE TRIGGER trg_tablename1_insert
AFTER INSERT ON TableName_1
    FOR EACH ROW
    EXECUTE PROCEDURE fnc_trg_tablename1_insert();
--
CREATE TRIGGER trg_tablename1_delete
AFTER DELETE ON TableName_1
    FOR EACH ROW
    EXECUTE PROCEDURE fnc_trg_tablename1_delete();

CREATE FUNCTION fnc_trg_tablename2_insert()
    RETURNS TRIGGER
    AS $tablename2_audit$
BEGIN
  IF (TG_OP = 'INSERT') THEN
        INSERT INTO tablename2_audit SELECT now(), 'I', NEW.*;
  END IF;
  RETURN NULL;
END;
$tablename2_audit$ LANGUAGE plpgsql;

CREATE TRIGGER trg_tablename2_insert
AFTER INSERT ON TableName_2
    FOR EACH ROW
    EXECUTE PROCEDURE fnc_trg_tablename2_insert();



CREATE OR REPLACE FUNCTION get_peer2_by_peer1(peer_inp VARCHAR)
    RETURNS VARCHAR
    AS $get_peer$
BEGIN
  RETURN (SELECT peer2 FROM TableName_1 WHERE peer1 = peer_inp LIMIT 1);
END;
$get_peer$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_recommended_peer(peer_inp VARCHAR)
  RETURNS VARCHAR
    AS 'SELECT RecommendedPeer FROM TableName_2 WHERE peer = peer_inp LIMIT 1'
    LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_last_check_date()
  RETURNS DATE
    AS 'SELECT Check_date FROM Checks ORDER BY Check_date DESC LIMIT 1'
    LANGUAGE SQL;



-- 1
CREATE OR REPLACE PROCEDURE drop_tables_wildcard(IN _schema TEXT, IN _parttionbase TEXT)
LANGUAGE plpgsql
AS
$$
DECLARE
    row record;
BEGIN
    FOR row IN
        SELECT
            table_schema,
            table_name
        FROM
            information_schema.tables
        WHERE
            table_type = 'BASE TABLE'
        AND
            table_schema = _schema
        AND
            table_name ILIKE (_parttionbase || '%')
    LOOP
        EXECUTE 'DROP TABLE ' || quote_ident(row.table_schema) || '.' || quote_ident(row.table_name) || ' CASCADE ';
    END LOOP;
END;
$$;

CALL drop_tables_wildcard('public', 'TableName');


-- 2
CREATE OR REPLACE PROCEDURE show_scalar_functions(OUT num_func INT, INOUT func_info refcursor) AS
$$
BEGIN
  SELECT COUNT(*) INTO num_func FROM
    (SELECT routines.routine_name, parameters.data_type, parameters.ordinal_position
    FROM information_schema.routines
    LEFT JOIN information_schema.parameters ON routines.specific_name=parameters.specific_name
    WHERE routines.specific_schema='public' AND parameters.data_type IS NOT NULL
    ORDER BY routines.routine_name, parameters.ordinal_position) a;
  OPEN func_info FOR
      SELECT routines.routine_name, parameters.data_type, parameters.ordinal_position
      FROM information_schema.routines
      LEFT JOIN information_schema.parameters ON routines.specific_name=parameters.specific_name
      WHERE routines.specific_schema='public' AND parameters.data_type IS NOT NULL
      ORDER BY routines.routine_name, parameters.ordinal_position;
END
$$ LANGUAGE plpgsql;

BEGIN;
CALL show_scalar_functions(NULL, 'func_info');
FETCH ALL FROM "func_info";
COMMIT;


-- 3
CREATE OR REPLACE PROCEDURE drop_all_triggers(OUT num_trig INT)
AS $$
DECLARE
  triggNameRecord RECORD;
  triggTableRecord RECORD;
  cnt INT := 0;
BEGIN
    FOR triggNameRecord IN select distinct(trigger_name) from information_schema.triggers where trigger_schema = 'public' LOOP
      FOR triggTableRecord IN SELECT distinct(event_object_table) from information_schema.triggers where trigger_name = triggNameRecord.trigger_name LOOP
          EXECUTE 'DROP TRIGGER ' || triggNameRecord.trigger_name || ' ON ' || triggTableRecord.event_object_table || ';';
          cnt := cnt + 1;
      END LOOP;
    END LOOP;
    num_trig := cnt;
    RETURN;
END;
$$ LANGUAGE plpgsql;

CALL drop_all_triggers(NULL);


-- 4
CREATE OR REPLACE PROCEDURE show_func_with_pattern(INOUT func_info refcursor,
                                                   IN pattern VARCHAR)
AS $$
BEGIN
OPEN func_info FOR
    SELECT proname::VARCHAR obj, prokind::VARCHAR type
    FROM (SELECT proname, prokind, prosrc
          FROM pg_catalog.pg_proc pr
          JOIN pg_catalog.pg_namespace ns
          ON pr.pronamespace = ns.oid
          WHERE ns.nspname = 'public' AND prokind != 'a') func_info
    WHERE prosrc LIKE CONCAT('%', $1, '%');
END;
$$ LANGUAGE plpgsql;

BEGIN;
CALL show_func_with_pattern('func_info', 'JOIN');
FETCH ALL FROM "func_info";
COMMIT;