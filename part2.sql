--2.1
CREATE
    OR REPLACE PROCEDURE insert_p2p(p_peer varchar,
                                    p_reviewer varchar,
                                    p_task varchar,
                                    review_status State
                                    )
    LANGUAGE plpgsql AS
$$
BEGIN
    IF review_status = 'Start' THEN
        INSERT INTO checks
        VALUES ((
                    SELECT MAX(id) + 1
                    FROM checks
                ),
                p_peer,
                p_task,
                current_date);
        INSERT INTO p2p
        VALUES ((SELECT MAX(id) + 1
                 FROM p2p),
                (SELECT MAX(id)
                 FROM checks),
                p_reviewer,
                review_status,
                current_time);
    ELSE
        INSERT INTO p2p
        VALUES ((
                    SELECT MAX(id) + 1
                    FROM p2p
                ),
                (
                    SELECT MAX(id)
                    FROM checks c
                    WHERE c.peer = p_peer
                      AND c.task = p_task
                      AND c."date" = current_date
                ),
                p_reviewer,
                review_status,
                current_time);
    END if;
END;
$$;
CALL insert_p2p(
        'Gamma',
        'Dpaleo',
        'C8_3DViewer_v1.0',
        'Start'::State
    );
-- 2.2
CREATE
    OR REPLACE PROCEDURE insert_verter(p_peer varchar,
                                       p_task varchar,
                                       review_status state,
                                       p_time time)
    LANGUAGE plpgsql AS
$$
BEGIN
    INSERT INTO Verter
    VALUES ((
                SELECT max(id) + 1
                FROM Verter
            ),
            (
                SELECT max(id)
                FROM Checks
                         RIGHT JOIN (
                    SELECT "Check"
                    FROM P2P
                    WHERE "state" = 'Success'
                ) as t ON Checks.id = "Check"
                WHERE Checks.peer = p_peer
                  AND Checks.task = p_task
            ),
            review_status,
            p_time);
END;
$$;
CALL insert_verter(
        'Dpaleo',
        'C5_s21_decimal',
        'Start'::state,
        '12:40:58'
    );
--2.3
CREATE
    OR REPLACE FUNCTION fnc_trg_p2p_start_insert() RETURNS TRIGGER AS
$p2p_start$
DECLARE
    cp     varchar;
    amount integer;
BEGIN
    IF
            TG_OP = 'INSERT'
            AND NEW.State = 'Start' THEN
        SELECT peer
        INTO cp
        FROM checks
        ORDER BY id DESC
        LIMIT 1;
        IF
            EXISTS(
                    SELECT *
                    FROM transferredpoints
                    WHERE checkingpeer = NEW.checkingpeer
                      AND checkedpeer = cp
                ) THEN
            SELECT pointsamount
            INTO amount
            FROM transferredpoints
            WHERE checkingpeer = NEW.checkingpeer
              AND checkedpeer = cp;
            UPDATE transferredpoints
            SET pointsamount = amount + 1
            WHERE checkingpeer = NEW.checkingpeer
              AND checkedpeer = cp;
        ELSE
            INSERT INTO transferredpoints
            VALUES ((
                        SELECT max(id) + 1
                        FROM transferredpoints
                    ),
                    NEW.checkingpeer,
                    cp,
                    1);
        END IF;
    END IF;
    RETURN NULL;
END;
$p2p_start$
    LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS trg_person_insert_audit ON p2p;
CREATE TRIGGER trg_person_insert_audit
    AFTER
        INSERT
    ON p2p
    FOR EACH ROW
EXECUTE PROCEDURE fnc_trg_p2p_start_insert();
CALL insert_p2p(
        'Gamma',
        'Dpaleo',
        'C8_3DViewer_v1.0',
        'Start'::State
    );
CALL insert_p2p(
        'Gamma',
        'Dpaleo',
        'C8_3DViewer_v1.0',
        'Failure'::State,
        '2023-01-13'
    );
--2.4
DROP FUNCTION fnc_trg_check_xp_insert();
DROP TRIGGER trg_check_xp_insert on xp;
CREATE
    OR REPLACE FUNCTION fnc_trg_check_xp_insert() RETURNS TRIGGER AS
$check_xp$
BEGIN
    IF (
               NEW.XPamount <= (
               SELECT maxxp
               FROM tasks
               WHERE title = (
                   SELECT task
                   FROM checks
                   WHERE id = NEW."Check"
               )
           )
           )
        AND (
           (
               WITH success_p2p AS (
                   SELECT p."Check" id
                   FROM p2p p
                   WHERE p."Check" = NEW."Check"
                     AND "State" = 'Success'
               ),
                    success_no_verter AS (
                        SELECT sp.id
                        FROM success_p2p sp
                                 LEFT JOIN Verter v ON sp.id = v."Check"
                        WHERE v."Check" IS NULL
                    ),
                    success_verter AS (
                        SELECT sp.id
                        FROM success_p2p sp
                                 JOIN Verter v ON sp.id = v."Check"
                        WHERE v."State" = 'Success'
                    ),
                    success_peer AS (
                        SELECT snv.id
                        FROM success_no_verter snv
                        UNION
                        SELECT sv.id
                        FROM success_verter sv
                    )
               SELECT id
               FROM success_peer
           ) IS NOT NULL
           ) THEN
        RETURN NEW;
    ELSE
        RETURN NULL;
    END IF;
END;
$check_xp$
    LANGUAGE plpgsql;
CREATE TRIGGER trg_check_xp_insert
    BEFORE
        INSERT
    ON XP
    FOR EACH ROW
EXECUTE PROCEDURE fnc_trg_check_xp_insert();
insert into XP
VALUES ('6', '6', '200');